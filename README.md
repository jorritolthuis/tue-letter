# TU/e LaTeX huisstijl
This repository contains the style file for creating letters similar to the TU/e September 2018 style in LaTeX. 
The current version is based on "Handboek visuele identiteit" v1, page 44. This document is available on [intranet.tue.nl](intranet.tue.nl).

The style file is based on the [brownletter](http://cs.brown.edu/about/system/managed/latex/) class created by Nesime Tatbul.

## Remaining work
For missing features in the template, see the [Issues](https://gitlab.com/jorritolthuis/tue-letter/issues) page. 
Please feel free to contribute to this style if anything is missing.