%
% TU/e letter style file.
% Please see https://gitlab.com/jorritolthuis/tue-letter
% Based on brownletter.cls by Nesime Tatbul
%

\ProvidesClass{tueletter}
\RequirePackage{graphicx}
\RequirePackage{ifthen}

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{letter}}
\ProcessOptions
\LoadClass[a4paper]{letter} % we always use a4 paper

\newcommand{\@subject}{}
\newcommand{\subject}[1]{\renewcommand{\@subject}{{\tiny Onderwerp}\\ #1}}

%\newcommand{\@contact}{}
%\newcommand{\contact}[1]{\renewcommand{\@contact}{{\tiny #1}}}
% Hasn't been implemented yet, usage:
%\contact{+31 (0) 40 247 9111\\info@tue.nl} % Optional, add your own contact information

% Set page alignments
\setlength{\textwidth}{6.875in}
\setlength{\textheight}{650pt} % Default = 550pt
\setlength{\topskip}{0.0in}
\setlength{\footskip}{1.0in}
\setlength{\oddsidemargin}{-0.25in}
\setlength{\evensidemargin}{-0.25in}
\setlength{\topmargin}{-0.875in}

%iflang package
%http://mirror.koddos.net/CTAN/macros/latex/contrib/oberdiek/iflang.pdf
% \IfLanguageName{ngerman}{Hallo}{Hello}

\newsavebox{\head}
\newsavebox{\foot}
\newsavebox{\emptyfoot}

% Header box, which contains TU/e information
\savebox{\head}{\tiny
    \begin{tabular*}{\textwidth}{@{}l@{\extracolsep{0.2\textwidth
    }}l@{\extracolsep{0.2\textwidth}}l@{\extracolsep{0.2\textwidth}}l@{}}
    TU/e Eindhoven          & Phone +31 123 4567 00 & Bank account 34.79.60.715\\
    Den Dolech 2            & www.tue.nl            & Rabobank Eindhoven \\
    P.O. Box 513            &                       & Swift RABONL2U \\
    NL-5600 MB Eindhoven    & Traderegister 11055439& IBAN NL90RABO034796071 \\
    The Netherlands         &                       & VAT NL811493635B01-32 \\
    \end{tabular*}
}

% Footer box, which contains some Latin text
\savebox{\foot}[\textwidth][c]{\tiny
    \begin{tabular*}{\textwidth}{l}
%    \ifthenelse{\equal{\@contact}{}}{{}}{\@contact\\}
%    \@contact
    Ullum simul tractatos mei ei, usu eirmod oportere intellegebat ea. Idque oratio laudem nam id, at alterum vocibus\\
    voluptua sed. Per et duis utamur volumus, his an utroque voluptatibus definitionem, ea debet noster pro. Ad vix delectus
    \end{tabular*}
}

% Footer box (empty), for all pages but the first
\savebox{\emptyfoot}[\textwidth][c]{
    \hspace*{\textwidth}
}

% Header and footer should be placed only on the first page
\renewcommand{\ps@firstpage}{
    \setlength{\headheight}{5em}
    \setlength{\headsep}{3em}
    \renewcommand{\@oddhead}{\usebox{\head}}
    \renewcommand{\@oddfoot}{\usebox{\foot}}
    \renewcommand{\@evenhead}{}
    \renewcommand{\@evenfoot}{}
}

\renewcommand{\ps@empty}{
    \setlength{\headheight}{0em} % There is only a header on the first page
    \setlength{\headsep}{8em}
    \renewcommand{\@oddhead}{}
    \renewcommand{\@oddfoot}{\usebox{\emptyfoot}}
    \renewcommand{\@evenhead}{}
    \renewcommand{\@evenfoot}{\usebox{\emptyfoot}}
}

\providecommand{\@evenhead}{}
\providecommand{\@oddhead}{}
\providecommand{\@evenfoot}{}
\providecommand{\@oddfoot}{}

\pagestyle{empty}

\renewcommand{\opening}[1]{\thispagestyle{firstpage}
    \begin{minipage}{0.80\textwidth}
    % Recipient
    {\tiny Retouradres: P.O. Box 513 \textbullet{} NL-5600 MB EINDHOVEN \textbullet{} The Netherlands \vspace{1em}} \\
    \toname \\ \toaddress
    \end{minipage}
    \begin{minipage}{0.25\textwidth}
    % TU/e Logo
    \vspace{1em}
    \includegraphics[width=\textwidth]{TUe-logo-square-descriptor-stack-scarlet-L.png}
    \end{minipage}
    \vspace{1\parskip}

    % Date and Reference
    \begin{tabular*}{\textwidth}{@{}l@{\extracolsep{2.5cm}}l@{}}
         {\tiny Datum}               & {\tiny Referentie} \\
         \raggedright\@date   & Eindhoven$\backslash$SLS$\backslash$09118....A2006
    \end{tabular*}
    
    % Subject
    \ifthenelse{\equal{\@subject}{}}{}{
    \vspace{1\parskip}\@subject\par}
    
    \vspace{5\parskip}
    #1\par\nobreak
}

\renewcommand{\closing}[1]{\par\nobreak\vspace{\parskip}%
    \stopbreaks % Make sure the page does not break here
    \ignorespaces #1\\
    [4\medskipamount] % Provide some room
    % Signature
    \fromsig
    \strut % Creates height with no width
    \par
}